<?php

namespace GKZF2\Model\Filter;

use Zend\InputFilter\InputFilter;
use Zend\ServiceManager\ServiceManager;

class ModelFilter extends InputFilter {
    /**
     * @var ServiceManager
     */
    protected $serviceManager;

    public function getServiceManager() {
        return $this->serviceManager;
    }

    public function __construct(ServiceManager $serviceManager) {
        $this->serviceManager = $serviceManager;
        $this->init();
    }

}