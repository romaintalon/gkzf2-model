<?php

namespace GKZF2\Model\Validator;

use GKZF2\Model\AbstractDAO;
use Zend\Validator\AbstractValidator;

class ForeignKey extends AbstractValidator
{

    /**
     * Invalid value
     */
    const INVALID_VALUE = 'foreignKeyValue';

    /**
     * Validation failure message template definitions
     *
     * @var array
     */
    protected $messageTemplates = array(
        self::INVALID_VALUE    => "The input is not valid",
    );

    /**
     * Default options to set for the validator
     *
     * @var mixed
     */
    protected $options = array(
        'dao'         => null,     // DAO used to get the model
    );

    /**
     * @return AbstractDAO
     */
    public function getDAO()
    {
        return $this->options['dao'];
    }

    /**
     * @param AbstractDAO $dao
     * @return $this
     */
    public function setDAO(AbstractDAO $dao)
    {
        $this->options['dao'] = $dao;
        return $this;
    }

    /**
     * Returns true if and only if the set DAO get
     * a model for the passed id
     *
     * @param  mixed $value
     * @param  mixed $context Additional context to provide to the callback
     * @return bool
     */
    public function isValid($value, $context = null)
    {
        $this->setValue($value);

        $dao = $this->getDAO();
        $model = $dao->getModelByUnikId($value);

        return ($model !== null);
    }
}
