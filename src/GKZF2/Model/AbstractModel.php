<?php

namespace GKZF2\Model;

use GKZF2\Core\ExchangeData\ExchangeData;
use GKZF2\Core\ExchangeData\ExchangeDataGetterInterface;

abstract class AbstractModel implements ExchangeDataGetterInterface {

    public static $SQL_PRIMARY_KEYS = null;
    const SQL_TABLE_NAME = null;

    public $added_date;
    public $updated_date;

    public function exchangeArray($data, $detectWithSqlTableNamePrefix = false, $postFixTableName = '') {
        
        foreach (get_object_vars($this) as $propertyName => $propertyValue) {
            
            if ($detectWithSqlTableNamePrefix) {
                $propertyNamePrefixed = $this::SQL_TABLE_NAME . ':::' . $propertyName . $postFixTableName;
                $this->$propertyName = (isset($data[$propertyNamePrefixed])) ? $data[$propertyNamePrefixed] : null;
            }
            else {
                $this->$propertyName = (isset($data[$propertyName])) ? $data[$propertyName] : null;
            }
        }
        
        return $this;
    }
    
    public function changeDataArrayExceptKeys($data) {
        
        foreach (get_object_vars($this) as $propertyName => $propertyValue) {
            
            // is not a primay key or is a key but a no auto-incremented key
            if (!array_key_exists($propertyName, $this::$SQL_PRIMARY_KEYS)
                    || (array_key_exists($propertyName, $this::$SQL_PRIMARY_KEYS)
                        && !$this::$SQL_PRIMARY_KEYS[$propertyName])
                    ) {
                // and should not be a date (automaticaly managed by mysql triggers)
                if ($propertyName != 'added_date' && $propertyName != 'updated_date') {
                    $this->$propertyName = (isset($data[$propertyName])) ? $data[$propertyName] : null;
                }
            }
        }
        
        return $this;
    }
    
    public function getArrayCopy() {
        return get_object_vars($this);
    }
    
    /**
     * 
     * Ignore __model_ properties
     * @return array
     */
    public function getArrayCopyForUpdate() {
        $updateArray = array();
        foreach (get_object_vars($this) as $propertyName => $propertyValue) {
            if (strpos($propertyName, '__model_') === false) {
                $updateArray[$propertyName] = $propertyValue;
            }
        }
        return $updateArray;
    }

    protected function postConversionMethod($formatedClientArray) {
        return $formatedClientArray;
    }

    /**
     * @param bool $useDateColumns
     * @param string $postFixTableName
     * @param null $restrict : list of columns to use. if null, use all columns
     * @return array
     */
    public function getFormattedArrayOfSqlFields($useDateColumns = true, $postFixTableName = '', $restrict = null) {

        $formatedSqlFieldArray = array();
        
        foreach (get_object_vars($this) as $propertyName => $propertyValue) {
            
            // We exclude by default properties with the __model_ prefix (that are AbstractModels and don't have any match in the database)
            if (strpos($propertyName, '__model_') === false) {
                
                if (
                    // We skip the date column is the input variable is false
                    (
                        (strcmp($propertyName, 'added_date') !== 0 && strcmp($propertyName, 'updated_date') !== 0)
                        || 
                        ((strcmp('added_date', $propertyName) === 0 || strcmp('updated_date', $propertyName) === 0)
                                && $useDateColumns)
                    )
                    AND
                    // We skip columns if restrict is not null
                    (
                        ($restrict === null || \array_search($propertyName, $restrict) !== false)
                    )

                ) {
                    
                    $key = $this::SQL_TABLE_NAME . ':::' . $propertyName . $postFixTableName;
                    $formatedSqlFieldArray[$key] = $propertyName;
                }
            }
        }
        
        return $formatedSqlFieldArray;
    }
    
    public function checkIfAllIdPropertiesAreDefined() {
        
        foreach ($this::$SQL_PRIMARY_KEYS as $key => $isAutoIncremented) {
            if (null === $this->$key
                    || !isset($this->$key)) {
                return false;
            }
        }
        
        return true;
    }

    /**
     * @return ExchangeData
     */
    public function getExchangeData() {
        $exchangeData = new ExchangeData();
        foreach (get_object_vars($this) as $propertyName => $propertyValue) {
            if (strpos($propertyName, '__model_') === false) {
                $exchangeData->addData($propertyName, $propertyValue);
            } else {
                if (isset($this->$propertyName)) {
                    if (strpos($propertyName, '_array_') !== false) {
                        $outputPropertyName = str_replace('__model_array_', '', $propertyName);
                        $exchangeData->addData($outputPropertyName, ExchangeData::exchangeDataGetterInterfaceListToExchangeData($this->$propertyName));
                    } else if (strpos($propertyName, '__model_alias_') === false) {
                        $outputPropertyName = str_replace('__model_', '', $propertyName);
                        $exchangeData->addData($outputPropertyName, $this->$propertyName->getExchangeData()->getData());
                    } else {
                        $outputPropertyName = str_replace('__model_alias_', '', $propertyName);
                        $exchangeData->addData($outputPropertyName, $this->$propertyName);
                    }
                }
            }
        }
        return $exchangeData;

    }
}
