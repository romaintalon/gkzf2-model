<?php

namespace GKZF2\Model\Db;

use GKZF2\Model\AbstractModel;
use Zend\ServiceManager\ServiceManager;

use Zend\Db\ResultSet\HydratingResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Stdlib\Hydrator\ObjectProperty;

class GenericDatabaseTools {
//
//    public function getGenericTableGateway(ServiceManager $serviceManager, AbstractModel $abstractModel) {
//        $resultSet = new ResultSet();
//        $resultSet->setArrayObjectPrototype($abstractModel);
//
//        /** @var \Zend\Db\Adapter\AdapterInterface $dbAdapter */
//        $dbAdapter = $serviceManager->get('Zend\Db\Adapter\Adapter');
//
//        return new TableGateway($abstractModel::SQL_TABLE_NAME, $dbAdapter, null, $resultSet);
//    }
    
    public function getHydratedTableGateway(ServiceManager $serviceManager, AbstractModel $abstractModel) {
        $hydratingResultSet = new HydratingResultSet();
        $hydratingResultSet->setHydrator(new ObjectProperty());
        $hydratingResultSet->setObjectPrototype($abstractModel);

        /** @var \Zend\Db\Adapter\AdapterInterface $dbAdapter */
        $dbAdapter = $serviceManager->get('Zend\Db\Adapter\Adapter');

        return new TableGateway($abstractModel::SQL_TABLE_NAME, $dbAdapter, null, $hydratingResultSet);
    }
}
