<?php

namespace GKZF2\Model\Db;

use GKZF2\Model\AbstractDAO;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Where;

class Query {
    protected $dao;
    /**
     *
     * @var \Zend\Db\Sql\Select
     */
    protected $select;

    /**
     * @var bool
     */
    protected $isTest = false;

    /**
     * @param AbstractDAO $abstractDAO
     * @param Select $select
     */
    public function __construct(AbstractDAO $abstractDAO, Select $select) {
        $this->dao = $abstractDAO;
        $this->select = $select;
    }
    
    /**
     * 
     * @param array $criterias
     * @return $this
     * @deprecated
     */
    public function addCriterias($criterias)
    {
        return $this->addCriteria($criterias);
    }

    /**
     * @param $criteria
     * @return $this
     */
    public function addCriteria($criteria) {
        foreach ($criteria as $field => $value) {
            if (is_array($value)) {
                if (array_key_exists('value', $value)) {
                    $v = $value['value'];
                } else {
                    $v = null;
                }
                $t = $value['test'];
                if (array_key_exists('field', $value)) {
                    $f = $value['field'];
                } else {
                    $f = $field;
                }
            } else {
                $v = $value;
                $t = 'equalTo';
                $f = $field;
            }
            $this->select->where(function(Where $where) use ($f, $t, $v) {
                $where->{$t}($f, $v);
            });
        }
        return $this;
    }

    /**
     * @param bool $isTest
     * @return $this
     */
    public function setIsTest($isTest) {
        $this->isTest = $isTest;
        return $this;
    }

    /** Ajoute une clause de tri.
     *
     * @param String $orderClause
     * @return $this
     * @see http://framework.zend.com/manual/current/en/modules/zend.db.sql.html#order
     */
    public function order($orderClause){
        $this->select->order($orderClause);
        return $this;
    }
    
    /**
     * @param array $options
     * @return $this
     */
    public function addOptions($options) {
        foreach ($options as $option => $value) {
            if ($value !== null) {
                $this->select->{$option}($value);
            }
        }
        return $this;
    }

    /**
     *
     * @param callable $method
     * @param mixed $args
     * @param bool $argIsArray
     * @return $this
     */
    public function callSelectOnMethod($method, $args = null, $argIsArray = false) {
        if ($args !== null) {
            if ($argIsArray) {
                array_unshift($args, $this->select);
                call_user_func_array($method, $args);
            } else {
                call_user_func($method, $this->select, $args);
            }
        } else {
            call_user_func($method, $this->select);
        }
        return $this;
    }

    /**
     *
     * @param callable $postTreatmentMethod
     * @return mixed
     */
    public function execute($postTreatmentMethod = null, $args = null, $argIsArray = false) {
        $result = $this->dao->selectWith($this->select, $this->isTest);
        if ($postTreatmentMethod !== null) {
            if ($args) {
                if ($argIsArray) {
                    array_unshift($args, $result);
                    return call_user_func_array($postTreatmentMethod, $args);
                } else {
                    return call_user_func($postTreatmentMethod, $result, $args);
                }
            } else {
                return call_user_func($postTreatmentMethod, $result);
            }
        }
        return $result;
    }
    
    /** Récupère la requête select. 
     * 
     * @return \Zend\Db\Sql\Select
     */
    public function getSelect(){
        return $this->select;
    }
}
