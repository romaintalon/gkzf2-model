<?php

namespace GKZF2\Model\ServiceLoader\GeneratorClass;

use GKZF2\Core\ServiceConfig\GeneratorClass\AbstractGeneratorClass;

class ModelGeneratorClass extends AbstractGeneratorClass {
    public $module;
    public $package;
    public $name;

    /**
     * @param string $module
     * @param string $name
     * @param string $package
     * @return ModelGeneratorClass
     */
    public static function create($module, $name, $package = null) {
        if (!$package) {
            $package = $name;
        }
        $modelGeneratorClass = new self();
        $modelGeneratorClass->module = $module;
        $modelGeneratorClass->name = $name;
        $modelGeneratorClass->package = $package;

        return $modelGeneratorClass;
    }


    /**
     * @return string
     */
    public function getServiceName()
    {
        return sprintf('%s\Model\%sDAO', $this->module, $this->name);
    }
}