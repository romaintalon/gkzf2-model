<?php

namespace GKZF2\Model\ServiceLoader\Generator;

use GKZF2\Core\ServiceConfig\Generator\AbstractGenerator;
use GKZF2\Core\ServiceConfig\GeneratorClass\AbstractGeneratorClass;
use GKZF2\Core\ServiceConfig\ServiceConfig;
use GKZF2\Model\AbstractModel;
use GKZF2\Model\Db\GenericDatabaseTools;
use GKZF2\Model\ServiceLoader\GeneratorClass\ModelGeneratorClass;
use Zend\ServiceManager\ServiceManager;

class ModelGenerator extends AbstractGenerator {

    /**
     * @param ServiceConfig $config
     * @param AbstractGeneratorClass|ModelGeneratorClass $generatorClass
     * @return $this
     */
    public function generate(ServiceConfig $config, AbstractGeneratorClass $generatorClass) {
        // generate DAO
        $config->addService(
            ServiceConfig::TYPE_FACTORIES, $generatorClass->getServiceName(),
            function(ServiceManager $sm) use ($generatorClass) {
                /** @var GenericDatabaseTools $dbTools */
                $dbTools = $sm->get('GKZF2\Model\Db\GenericDatabaseTools');
                $modelClass = sprintf('%s\Model\%s\%sModel', $generatorClass->module, $generatorClass->package, $generatorClass->name);
                /** @var AbstractModel $model */
                $model = new $modelClass();
                $daoClass = sprintf('\%s\Model\%s\%sDAO', $generatorClass->module, $generatorClass->package, $generatorClass->name);
                return new $daoClass($sm, $dbTools->getHydratedTableGateway($sm, $model), $model::$SQL_PRIMARY_KEYS);
            }
        );

        return $this;
    }

}
