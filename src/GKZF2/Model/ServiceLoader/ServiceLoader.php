<?php

namespace GKZF2\Model\ServiceLoader;

use GKZF2\Core\Exception\ServiceLoaderNotInitializedException;
use GKZF2\Core\ServiceConfig\ServiceConfig;
use GKZF2\Core\ServiceLoader\ServiceLoaderInterface;
use GKZF2\Model\ServiceLoader\Generator\ModelGenerator;
use GKZF2\Model\ServiceLoader\GeneratorClass\ModelGeneratorClass;

abstract class ServiceLoader implements ServiceLoaderInterface {
    protected $_models = null;

    /**
     * Must set the $_models value to an array
     * @param ModelGeneratorClass[] $models
     * @return $this
     */
    abstract public function setModels(array $models);

    /**
     * @param ServiceConfig $config
     * @return mixed|void
     * @throws ServiceLoaderNotInitializedException
     */
    public function loadServices(ServiceConfig $config) {
        if ($this->_models === null || !is_array($this->_models)) {
            throw new ServiceLoaderNotInitializedException();
        }

        $modelGenerator = new ModelGenerator();
        foreach ($this->_models as $model) {
            $modelGenerator->generate($config, $model);
        }
    }
}