<?php

namespace GKZF2\Model;

use Zend\Db\Sql\Select;
use Zend\ServiceManager\ServiceManager;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Expression;

use GKZF2\Model\Db\Query;

class AbstractDAO {
    
    private $serviceManager;
    
    private $tableGateway;
    private $dbAdapter;
    
    private $idColumnNameArray;

    /**
     * The last input ''$idColumnNameArray'' is the list of the primary key fields in the database
     * 
     * Ex. :    if there is a couple of primary keys (id_1 and id_2) that are both not auto-incremented:
     *          ''$idColumnNameArray'' has to be defined as:
     * 
     * array(
     *      'id_1' => false,
     *      'id_2' => false,
     * )
     *  
     * If this input is null / empty : the default primary key is ''id'' and it's defined as auto-incremented
     * So the default array will be: 
     * 
     * array(
     *      'id' => true,
     * )
     * 
     * @param ServiceManager $serviceManager
     * @param TableGateway $tableGateway
     * @param array $idColumnNameArray
     */
    function __construct(ServiceManager $serviceManager, TableGateway $tableGateway, $idColumnNameArray = array()) {

        $this->serviceManager = $serviceManager;
        
        $this->tableGateway = $tableGateway;
        $this->dbAdapter = $serviceManager->get('Zend\Db\Adapter\Adapter');
        
        if (empty($idColumnNameArray)) {
            $defaultIdName = 'id';
            $idColumnNameArray[$defaultIdName] = true; // by default: we guess it's auto-incremented...
        }
        
        $this->idColumnNameArray = $idColumnNameArray;
    }

    /**
     * 
     * @return TableGateway
     */
    protected function getTableGateway() {
        return $this->tableGateway;
    }
    
    /**
     * 
     * @return \Zend\Db\Adapter\Adapter
     */
    protected function getDbAdapter() {
        return $this->dbAdapter;
    }
    
    public function getServiceManager() {
        return $this->serviceManager;
    }

    /**
     * Simply returns a Zend\Db\Sql\Expression
     *
     * Just to avoid defining the Zend\Db\Sql\Expression in the concrete class...
     *
     * @param string $expression
     * @param string|array $parameters
     * @param array $types
     * @return \Zend\Db\Sql\Expression
     */
    protected function getSqlExpression($expression = '', $parameters = null, $types = array()) {
        return new Expression($expression, $parameters, $types);
    }
    
    /**
     * Test if the list of column name has exactly 1 element and returns its value 
     * (should be true if auto-incremented)
     * 
     * @return boolean
     */
    protected function isAnAutoIncrementedId() {
        
        return (!empty($this->idColumnNameArray) 
                && count($this->idColumnNameArray) === 1) ? reset($this->idColumnNameArray) : false;
    }
    
    /**
     * 
     * Returns an array set with the list of primary key values of for the given AbstractModel
     * 
     * Returns false if one of the AbstractModel's property does not have all the primary keys 
     * defined in the $idColumnNameArray...
     * 
     * @param AbstractModel $model
     * @return array or false
     */
    protected function getIdColumnNameValueArray(AbstractModel $model) {
        $modelArray = $model->getArrayCopy();

        $idColumnNameValueArray = array();
        foreach ($this->idColumnNameArray as $propertyName => $propertyValue) {
            if (array_key_exists($propertyName, $modelArray)) {
                $idColumnNameValueArray[$propertyName] = $modelArray[$propertyName];
            }
        }
        
        return (count($this->idColumnNameArray) === count($idColumnNameValueArray)) ? $idColumnNameValueArray : false;
    }
    
    /**
     * 
     * You can use this method if the class contains only one primary key 
     * 
     * If you use it even if there are more than 1 keys, this method returns false
     * 
     * @param mixed $idValue
     * @return AbstractModel|null
     */
    public function getModelByUnikId($idValue) {
        if (!empty($this->idColumnNameArray) 
                && count($this->idColumnNameArray) === 1) {
            
            reset($this->idColumnNameArray);
            $idColumnName = key($this->idColumnNameArray);
            
            return $this->getModelByField($idColumnName, $idValue);
        }

        return null;
    }

    /**
     * @param $idColumnNameValueArray
     * @return bool|AbstractModel
     */
    public function getModelByIdArray($idColumnNameValueArray) {
        if (count($this->idColumnNameArray) === count($idColumnNameValueArray)) {

            $isIdArrayOk = true;
            
            foreach ($idColumnNameValueArray as $idName => $idValue) {
                if (!array_key_exists($idName, $this->idColumnNameArray)) {
                    $isIdArrayOk = false;
                }
            }
            
            return $isIdArrayOk ? $this->getModelByFieldArray($idColumnNameValueArray) : false;
        }
        
        return false;        
    }
    
    /**
     * Can be used easily to do a select query on a specific property
     * 
     * @param string $fieldName
     * @param mixed $fieldValue
     * @return AbstractModel or false
     */
    public function getModelByField($fieldName, $fieldValue) {
        return $this->getModelByFieldArray(array($fieldName => $fieldValue));
    }
    
    /**
     * Can be used easily to return a specific model according to a list of fields
     * 
     * @param array $fieldsArray
     * @return AbstractModel or false
     */
    public function getModelByFieldArray($fieldsArray, $test = FALSE) {
        
        $select = $this->tableGateway->getSql()->select();
        $select->where($fieldsArray);
       
        $resultSet = $this->tableGateway->selectWith($select);
              
        if ($test) {
            $sql = $this->tableGateway->getSql();
            echo $sql->buildSqlString($select); die();
        }

        foreach ($resultSet as $r) {
            return $r;
        }

        return null;
//        return $resultSet->current();
    }
    
    public function createQuery() {
        return new Query($this, $this->tableGateway->getSql()->select());
    }

    /**
     * Save into the database and
     * 
     * automatically detect if it's an auto-incremented model or defined by a list of primary keys
     * to return the correct recently updated model
     * 
     * @param AbstractModel $model
     * @return AbstractModel or false
     */
    public function createModel(AbstractModel $model) {
        $idArray = $this->getIdColumnNameValueArray($model);
        
        if ($idArray) {
        
            $this->tableGateway->insert($model->getArrayCopyForUpdate());
        
            if ($this->isAnAutoIncrementedId()) {

                return $this->getModelByUnikId($this->tableGateway->lastInsertValue);
            }
            else {

                return $this->getModelByIdArray($idArray);
            }
        }
        
        return false;
    }

    /**
     * @param AbstractModel[] $modelArray
     * @return bool|\Zend\Db\Adapter\Driver\ResultInterface
     */
    public function createModels(array $modelArray) {
        if (count($modelArray) > 0) {

            /** @var AbstractModel $model */
            $model = $modelArray[0];
            $query = 'INSERT INTO ' . $model::SQL_TABLE_NAME . ' (';
            
            $blankParameters = '(';
            foreach ($model->getArrayCopyForUpdate() as $propertyName => $propertyValue) {
                $query = $query . $propertyName . ',';
                
                $blankParameters = $blankParameters . '?,';
            }
            
            $query = rtrim($query, ',') . ') VALUES ';
            $blankParameters = rtrim($blankParameters, ',') . '), ';
            
            $parameters = array();
            foreach ($modelArray as $model) {
                
                $query = $query . $blankParameters;
                
                foreach ($model->getArrayCopyForUpdate() as $propertyName => $propertyValue) {
                    array_push($parameters, $propertyValue);
                }
            }
            
            $query = rtrim($query, ', ') . ';';
        
            $statement = $this->getDbAdapter()->createStatement($query, $parameters);
            $result = $statement->execute();
            
            return $result;
        }
        
        return false;
    }

    /**
     * Update into the database and
     * 
     * automatically detect if it's an auto-incremented model or defined by a list of primary keys
     * to return the correct recently updated model
     * 
     * @param AbstractModel $model
     * @return AbstractModel or false
     */
    public function updateModel(AbstractModel $model) {
        
        $idArray = $this->getIdColumnNameValueArray($model);
        
        if ($idArray) {
            $this->tableGateway->update($model->getArrayCopyForUpdate(), $idArray);
            return $this->getModelByIdArray($idArray);
        } 
        
        return false;
    }

    /**
     * 
     * @param AbstractModel[] $models
     * @return array
     */
    public function updateModels($models) {
        // TODO : only one request ?
        $result = array();
        foreach ($models as $model) {
            $r = $this->updateModel($model);
            if ($r === false) {
                // one has failed ? should we stop and return false ?
            
            } else {
                $result[] = $r;
            }
        }
        return $result;
    }
    
    /**
     * Remove AbstractModel from the database
     * 
     * @param AbstractModel $model
     * @return boolean
     */
    public function deleteModel(AbstractModel $model) {
        
        $idArray = $this->getIdColumnNameValueArray($model);
        
        if ($idArray) {
            $this->tableGateway->delete($idArray);
            return true;
        }
        
        return false;
    }
    
    public function deleteModels($models) {
        // TODO : only one request ?
        foreach ($models as $model) {
            if (!$this->deleteModel($model)) {
                return false;
            }
        }
        return true;
    }

    /**
     * @param Select $select
     * @param bool $test
     * @return null|\Zend\Db\ResultSet\ResultSetInterface
     */
    public function selectWith(Select $select, $test) {
        if ($test) {
         $sql = $this->tableGateway->getSql();
//         echo get_class($sql);
         echo $sql->getSqlStringForSqlObject($select); die();
        }
        return $this->tableGateway->selectWith($select);
    }

    /**
     * @param string $method
     * @return array
     */
    public function createCallback($method) {
        return array($this, $method);
    }
}
